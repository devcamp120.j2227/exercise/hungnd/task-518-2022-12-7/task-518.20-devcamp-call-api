import { Component } from "react";

import 'bootstrap/dist/css/bootstrap.min.css'

class FetchApi extends Component {

    //sử dụng async await
    fetchApi = async (url, body) => {
        const response = await fetch(url, body);

        const data = await response.json();

        return data;
    }
    constructor(props) {
        super(props);
        this.state = {
            color1: "#2980b9",
            color2: "#2980b9",
            color3: "#2980b9",
            color4: "#2980b9",
            color5: "#2980b9"
        }
    }

    getDetailBtnHandler = () => {
        this.fetchApi("https://jsonplaceholder.typicode.com/posts/1")
            .then((data) => {
                console.log(data)
            })
            .catch((error) => {
                console.log(error.message)
            })
        this.setState({
            color1: "#27ae60",
            color2: "#2980b9",
            color3: "#2980b9",
            color4: "#2980b9",
            color5: "#2980b9"
        })
    }

    getAllBtnHandler = () => {
        console.log("Get All: ");

        //Call api get all sử dụng fetch
        // fetch("https://jsonplaceholder.typicode.com/posts")
        //     .then((response) => {
        //         return response.json();
        //     })
        //     .then((data)=>{
        //         console.log(data)
        //     })
        //     //Bắt lỗi
        //     .catch((error)=>{
        //         console.log(error.message)
        //     })

        this.fetchApi("https://jsonplaceholder.typicode.com/posts")
            .then((data) => {
                console.log(data)
            })
            .catch((error) => {
                console.log(error.message)
            })
        this.setState({
            color1: "#2980b9",
            color2: "#27ae60",
            color3: "#2980b9",
            color4: "#2980b9",
            color5: "#2980b9"
        })
    }

    createBtnHandler = () => {
        const body = {
            method: "POST",
            body: JSON.stringify({
                id: 1,
                title: "foo",
                body: "bar",
                userId: 1
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },

        }
        this.fetchApi("https://jsonplaceholder.typicode.com/posts/", body)
            .then((data) => {
                console.log(data)
            })
            .catch((error) => {
                console.log(error.message)
            })
        this.setState({
            color1: "#2980b9",
            color2: "#2980b9",
            color3: "#27ae60",
            color4: "#2980b9",
            color5: "#2980b9"
        })
    }

    updateBtnHandler = () => {
        const body = {
            method: 'PUT',
            body: JSON.stringify({
                title: 'foo',
                body: 'bar',
                userId: 1,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }

        this.fetchApi("https://jsonplaceholder.typicode.com/posts/1", body)
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.log(error.message);
            })
        this.setState({
            color1: "#2980b9",
            color2: "#2980b9",
            color3: "#2980b9",
            color4: "#27ae60",
            color5: "#2980b9"
        })

    }

    deleteBtnHandler = () => {
        const body = {
            method: 'DELETE'
        }

        this.fetchApi("https://jsonplaceholder.typicode.com/posts/1", body)
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.log(error.message);
            })
        this.setState({
            color1: "#2980b9",
            color2: "#2980b9",
            color3: "#2980b9",
            color4: "#2980b9",
            color5: "#27ae60"
        })
    }

    render() {
        return (
            <>
                <div className="row mt-5">
                    <h3>Fetch Api</h3>
                </div>
                <div className="row mt-5">
                    <div className="col">
                        <button className="btn" style={{ backgroundColor: this.state.color1 }} onClick={this.getDetailBtnHandler}>Get detail</button>
                    </div>
                    <div className="col">
                        <button className="btn" style={{ backgroundColor: this.state.color2 }} onClick={this.getAllBtnHandler}>Get all</button>
                    </div>
                    <div className="col">
                        <button className="btn" style={{ backgroundColor: this.state.color3 }} onClick={this.createBtnHandler}>Create</button>
                    </div>
                    <div className="col">
                        <button className="btn" style={{ backgroundColor: this.state.color4 }} onClick={this.updateBtnHandler}>Update</button>
                    </div>
                    <div className="col">
                        <button className="btn" style={{ backgroundColor: this.state.color5 }} onClick={this.deleteBtnHandler}>Delete</button>
                    </div>
                </div>

            </>
        )
    }
}
export default FetchApi;